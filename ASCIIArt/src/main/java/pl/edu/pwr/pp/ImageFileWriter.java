package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		
		PrintWriter printWriter = null;
		try {
		    printWriter = new PrintWriter(fileName, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException error) {
		    error.printStackTrace();
		}
		int rows = ascii.length;
		int columns = ascii[0].length;
		for(int i=0;i<rows;i++){
			String text = "";
			for(int j=0;j<columns;j++){
				text += ascii[i][j] ;
			}
			printWriter.println(text);
		}
		printWriter.close();
		
	}
	
}
